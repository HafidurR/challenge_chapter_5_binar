require('dotenv').config();
const express       = require('express');
const morgan        = require('morgan');
const app           = express();
const port          = process.env.PORT || 3000;
const authors       = require('./routes/author');
const books         = require('./routes/book');
const categories    = require("./routes/category");
const users         = require("./routes/user");
const swaggerJSON   = require('./api-docs/swagger-output.json');
const swaggerUI     = require('swagger-ui-express');

app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/authors', authors);
app.use('/books', books);
app.use("/categories", categories);
app.use("/users", users);

app.listen(port, () => {
  console.log(`Running on port ${port}`);
});

// module.exports = app;

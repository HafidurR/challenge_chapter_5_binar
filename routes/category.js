const router        = require('express').Router();
const categoryRoute = require('../controller/categoryController');
const verifyToken   = require('../middlewares/verifyToken');

router.get('/', categoryRoute.getAll);
router.get('/:id', verifyToken, categoryRoute.getByID);
router.post('/', verifyToken,  categoryRoute.create);
router.put('/:id', verifyToken, categoryRoute.update);
router.delete('/:id', verifyToken, categoryRoute.delete);

module.exports = router;
const { Category } = require('../models');

class categoryController {
  static getAll = async (req, res) => {
    let { page, row } = req.query;

    page -= 1;
    await Category.findAll({
      attributes: ['id', 'category_name'],
      offset: page,
      limit: row
    })
      .then((result) => {
        return res.status(200).json({
          message: 'success get all data',
          category: result
        })
      })
      .catch((err) => {
        return res.status(500).json({
          status: 'error',
          message: err.message
        })
      })
  }

  static getByID = async (req, res) => {
    const id = req.params.id;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Category.findOne({
        where: {
          id: id
        }
      })
        .then((result) => {
          if (result === null) {
            return res.status(404).json({
              status: 'error',
              message: 'Data not found'
            })
          } else {
            return res.status(200).json({
              message: 'success get by ID',
              category: result
            })
          }
        })
        .catch((err) => {
          return res.status(500).json({
            status: 'error',
            message: err.message
          })
        })
    }
  }

  static create = async (req, res) => {
    const { category_name } = req.body;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Category.create({
        category_name
      })
        .then((result) => {
          return res.status(201).json({
            message: 'success create new category',
            category: result
          })
        })
        .catch((error) => {
          const err = error.errors
          const errorList = err.map(d => {
            let obj = {}
            obj[d.path] = d.message
            return obj;
          })
          return res.status(400).json({
            status: 'error',
            message: errorList
          })
        })
    }
  }

  static update = async (req, res) => {
    const id = req.params.id;
    const { category_name } = req.body;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Category.findOne({
        where: {
          id: id
        }
      })
        .then(async (result) => {
          if (result === null) {
            return res.status(404).json({
              status: 'error',
              message: 'Data not found'
            })
          } else {
            await Category.update({
              category_name
            }, {
              where: {
                id: id
              }
            })
              .then(async () => {
                await Category.findOne({
                  where: {
                    id: id
                  }
                })
                  .then(rsl => {
                    return res.status(200).json({
                      message: 'success update category',
                      Category: rsl
                    })
                  }).catch(err => {
                    return res.status(400).json({
                      message: err.message
                    })
                  })
              })
              .catch((error) => {
                return res.status(400).json({
                  status: 'error',
                  message: error.message
                })
              })
          }
        })
        .catch((error) => {
          return res.status(500).json({
            status: 'error',
            message: error.message
          })
        })
    }
  }

  static delete = async (req, res) => {
    const id = req.params.id;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Category.findOne({
        where: {
          id: id
        }
      })
        .then(async (result) => {
          if (result === null) {
            return res.status(404).json({
              status: 'error',
              message: 'Data not found'
            })
          } else {
            await Category.destroy({
              where: {
                id: id
              }
            })
              .then(() => {
                return res.status(200).json({
                  message: 'success delete category',
                })
              })
              .catch((err) => {
                return res.status(400).json({
                  status: 'error',
                  message: err.message
                })
              })
          }
        })
        .catch((err) => {
          return res.status(500).json({
            status: 'error',
            message: err.message
          })
        })
    }
  }
}

module.exports = categoryController;
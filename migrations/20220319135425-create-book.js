'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('books', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      publisher: {
        type: Sequelize.STRING
      },
      publish_at: {
        type: Sequelize.DATE
      },
      author_id: {
        allowNull: false,
        references: {
          model: "authors",
          key: "id",
        },
        type: Sequelize.INTEGER
      },
      category_id: {
        allowNull: false,
        references: {
          model: "categories",
          key: "id",
        },
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('books');
  }
};